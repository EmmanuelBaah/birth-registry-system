from django.urls import path
from .views import *

urlpatterns = [
    path('center/signup/', CenterSignUp.as_view()),
    path('all/list/', AllUserList.as_view()),
    path('center/admins/', CenterAdmins.as_view()),
    path('center/admin/search/', CenterAdminsSearch.as_view()),
    path('registrar/admins/', RegistrarAdmins.as_view()),
    path('registrar/admins/search/', RegistrarAdminsSearch.as_view()),
    path('list/', UserList.as_view()),
    path('center/signin/', CenterSignIn.as_view()),
    path('registrar/signin/', RegistrarSignIn.as_view()),
    path('center/user/create/', CreateCenterUser.as_view()),
    path('registrar/user/create/', CreateRegistrarUser.as_view()),
    path('activate/', ActivateUser.as_view()),
    path('center/subs/', CenterSubAdminsAnnex.as_view()),
    path('center/subs/search/', CenterSubAdminsSearch.as_view()),
    path('details/<user_id>/', UserDetail.as_view()),
]
