from django.contrib.auth.models import AnonymousUser
from rest_framework.permissions import BasePermission


class IsSuperUserPermissions(BasePermission):
    """
    Allows access to superusers
    """

    def has_permission(self, request, view):
        if request.user == AnonymousUser():
            return False

        if request.user.is_superuser:
            return True
        else:
            return False


class GeneralUserPermissions(BasePermission):
    """
    Allows access to all user types except Anonymous user
    """

    def has_permission(self, request, view):
        if request.user == AnonymousUser():
            return False
        return True


class NurseUserPermissions(BasePermission):
    """
    Allows access to only Nurse Users
    """

    def has_permission(self, request, view):
        if request.user == AnonymousUser():
            return False

        if request.user.user_type == 'CNU':
            return True
        else:
            return False


class CenterUserPermissions(BasePermission):
    """Allows access to all center users"""

    def has_permission(self, request, view):
        if request.user == AnonymousUser():
            return False

        if request.user.user_type == 'CNU' or request.user.user_type == 'CAU':
            return True
        else:
            return False


class CenterAdminUserPermissions(BasePermission):
    """
    Allows access to only Center Account Creators
    """

    def has_permission(self, request, view):
        if request.user == AnonymousUser():
            return False

        if request.user.user_type == 'CAU':
            return True
        else:
            return False


class RegistrarSuperUserPermissions(BasePermission):
    """
    Allows access to only registrar super users
    """

    def has_permission(self, request, view):
        if request.user == AnonymousUser():
            return False

        if request.user.user_type == 'RSU':
            return True
        else:
            return False


class RegistrarUserPermissions(BasePermission):
    """
    Allows access to registrar users
    """

    def has_permission(self, request, view):
        if request.user == AnonymousUser():
            return False

        if request.user.user_type == 'RU' or request.user.user_type == 'RSU':
            return True
        else:
            return False


class RegistrarSubPermissions(BasePermission):
    """
    Allows access to registrar users
    """

    def has_permission(self, request, view):
        if request.user == AnonymousUser():
            return False

        if request.user.user_type == 'RU':
            return True
        else:
            return False
