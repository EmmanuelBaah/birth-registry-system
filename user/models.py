from random import randrange

from django.db import models
from django.contrib.auth.models import BaseUserManager, AbstractBaseUser, PermissionsMixin
from django.utils.translation import ugettext_lazy as _
from django.utils import timezone


class BRUserManager(BaseUserManager):
    def create_user(self, username, first_name, last_name, password=None):
        if not username:
            raise ValueError('Users must have a username')

        user = self.model(
            username=username,
            first_name=first_name,
            last_name=last_name
        )

        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, username, password, first_name, last_name):
        user = self.create_user(username, password=password, first_name=first_name, last_name=last_name)
        user.is_superuser = True
        user.is_admin = True
        user.save(using=self._db)
        return user


def generate_user_id():
    FROM = '0123456789'
    LENGTH = 7
    user_id = ""
    for i in range(LENGTH):
        user_id += FROM[randrange(0, len(FROM))]
    return f'MBR{user_id}'


TODAY = timezone.now()
DATE_FORMAT = TODAY.strftime("%d-%m-%Y %H:%M:%S")
USER_DIR = 'NEW_CACHES/AVATARS/' + DATE_FORMAT + '/'
SEX = {
    (1, 'Male'),
    (2, 'Female'),
    (3, 'Other')
}

USER_TYPE = {
    ('CAU', 'Center Admin User'),
    ('CNU', 'Center Nurse User'),
    ('RSU', 'Registrar Super User'),
    ('WNU', 'Web Normal User'),
    ('RU', 'Registrar User'),
}


class BRUser(AbstractBaseUser, PermissionsMixin):

    user_id = models.CharField(primary_key=True, default=generate_user_id, max_length=10)
    username = models.CharField(
        _('username'),
        unique=True,
        max_length=150
    )
    email = models.EmailField(
        _('email_address'),
        max_length=255,
        blank=True,
        null=True
    )
    first_name = models.CharField(_('First Name'), max_length=150, blank=True, null=True)
    last_name = models.CharField(_('Last Name'), max_length=150, blank=True, null=True)
    position = models.CharField(_('Position'), max_length=255, blank=True, null=True)
    address = models.CharField(_('Address of Residence'), max_length=255, blank=True, null=True)
    department = models.CharField(_('Department'), max_length=255, blank=True, null=True)
    is_active = models.BooleanField(default=True)
    is_admin = models.BooleanField(default=False)
    is_superuser = models.BooleanField(default=False)
    created_at = models.DateTimeField(auto_now_add=True)
    center_id = models.CharField(max_length=255, blank=True, null=True)
    center_name = models.CharField(max_length=255, blank=True, null=True)
    user_type = models.CharField(_('User Type'), max_length=255, choices=USER_TYPE, default='WNU', blank=True, null=True)
    sex = models.CharField(_('Gender'), max_length=255, blank=True, null=True)
    branch = models.CharField(_('Branch'), max_length=255, blank=True, null=True)
    region = models.CharField(_('Region'), max_length=255, null=True)
    date_of_birth = models.DateField(blank=True, null=True)
    date_joined = models.DateTimeField(_('Date Joined'), default=timezone.now)
    login_number = models.IntegerField(_('Number of Login'), default=0, blank=True, null=True)

    objects = BRUserManager()

    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = ['first_name', 'last_name']

    def get_full_name(self):
        full_name = f'{self.first_name} {self.last_name}'
        return full_name.strip()

    def get_short_name(self):
        return self.first_name

    def get_dob(self):
        return self.date_of_birth

    @property
    def is_staff(self):
        return self.is_admin

    class Meta:
        ordering = ('user_id', 'first_name', )
        verbose_name_plural = _('Birth Register users')
        verbose_name = _('Birth Register user')

    def __str__(self):
        return '%s, %s' % (self.user_id, self.get_full_name())
