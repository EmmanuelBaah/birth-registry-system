# Generated by Django 2.2.1 on 2019-06-09 19:25

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('birth', '0003_birth_registry_center'),
    ]

    operations = [
        migrations.AddField(
            model_name='birth',
            name='margin',
            field=models.CharField(blank=True, max_length=255, null=True),
        ),
    ]
