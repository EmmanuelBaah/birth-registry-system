from django.urls import path
from .views import *

urlpatterns = [
    path('center/list/', CenterList.as_view()),
    path('center/update/<id>/', CenterUpdate.as_view()),
    path('center-detail/<id>/', CenterDetail.as_view()),

    path('center/guardian/create/', CreateCenterGuardian.as_view()),
    path('registrar/guardian/create/', CreateRegistrarGuardian.as_view()),
    path('center/guardian/list/', CenterGuardianList.as_view()),
    path('guardian/list/', AllGuardianList.as_view()),
    # path('center/guardian/update/<id>/', CenterGuardianUpdate.as_view()),
    path('annex/center/guardian/update/<id>/',  CenterGuardianUpdate.as_view()),
    # path('guardian/update/<id>/', RegistrarGuardianUpdate.as_view()),
    path('annex/guardian/update/<id>/', RegistrarGuardianUpdate.as_view()),

    path('doctor/create/', CreateDoctor.as_view()),
    path('doctor/list/', DoctorList.as_view()),
    path('doctor/update/<id>/', DoctorUpdate.as_view()),

    path('search/', BirthSearch.as_view()),
    path('center/search/', CenterBirthSearch.as_view()),
    path('center/guardian/search/', CenterGuardianSearch.as_view()),
    path('registrar/guardian/search/', GuardianSearch.as_view()),

    path('center/create/', CreateCenterBirth.as_view()),
    path('registrar/create/', CreateRegistrarBirth.as_view()),
    path('list/', CenterBirthList.as_view()),
    path('cert/list/', BirthCert.as_view()),
    path('registrar/list/', AllBirthList.as_view()),
    path('center/detail/<id>/', CenterBirthDetail.as_view()),
    path('registrar/detail/<id>/', RegistrarBirthDetail.as_view()),
    # path('center/update/<id>/', CenterBirthUpdate.as_view()),
    path('annex/center/edit/<id>/', CenterBirthUpdate.as_view()),
    # path('registrar/update/<id>/', RegistrarBirthUpdate.as_view()),
    path('annex/registrar/edit/<id>/', RegistrarBirthUpdate.as_view()),
    path('filter/', FilterBirth.as_view()),
    path('registrar/generate/<id>/', GenerateBirthCert.as_view()),
    path('stats/', ShowBirthStats.as_view()),
]
