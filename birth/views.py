from django.db.models import Q
from rest_framework.views import *
from rest_framework.generics import *
from rest_framework.permissions import *

from user.models import BRUser
from user.user_permissions import *
from .serializers import *
from .models import *
from pyhubtel_sms import SMS, Message


class CenterList(ListAPIView):
    permission_classes = (RegistrarUserPermissions, RegistrarSuperUserPermissions, IsSuperUserPermissions)
    serializer_class = CenterSerializer
    queryset = MedicalCenter.objects.all()


class CenterDetail(RetrieveAPIView):
    permission_classes = (CenterUserPermissions,)
    serializer_class = CenterSerializer
    lookup_field = 'id'

    def get_queryset(self):
        return MedicalCenter.objects.filter(created_by=self.request.user, id=self.request.user.center_id)

    def get(self, request, *args, **kwargs):
        return self.retrieve(self, request, *args, **kwargs)


class CenterUpdate(UpdateAPIView):
    permission_classes = (CenterAdminUserPermissions,)
    serializer_class = CenterSerializer
    lookup_field = 'id'

    def get_queryset(self):
        return MedicalCenter.objects.filter(created_by=self.request.user)


class CenterGuardianSearch(APIView):
    permission_classes = (CenterUserPermissions, )

    def post(self, request):
        search = request.data.get('search', None)

        if search is not None:
            center_id = self.request.user.center_id
            results = Guardian.objects.filter(created_by_center__is_active=True, created_by_center__id=center_id)
            for term in search.split():
                results = results.filter(Q(first_name__icontains=term)
                                         | Q(last_name__icontains=term)
                                         | Q(other_name__icontains=term)
                                         | Q(region__icontains=term)
                                         | Q(town_or_village__icontains=term)
                                         | Q(nationality__icontains=term))
                if not results:
                    return Response({'results': 'Your search was not found', 'response_code': '101'},
                                    status=status.HTTP_400_BAD_REQUEST)
                serializer = GuardianSerializer(results, context={'request': request}, many=True)
                return Response({'results': serializer.data, 'response_code': '100'}, status=status.HTTP_200_OK)
            return Response({'detail': 'Search Text is empty', 'response_code': '102'},
                            status=status.HTTP_400_BAD_REQUEST)


class GuardianSearch(APIView):
    permission_classes = (RegistrarUserPermissions,)

    def post(self, request):
        search = request.data.get('search', None)

        if search is not None:
            results = Guardian.objects.all()
            for term in search.split():
                results = results.filter(Q(first_name__icontains=term)
                                         | Q(last_name__icontains=term)
                                         | Q(other_name__icontains=term)
                                         | Q(region__icontains=term)
                                         | Q(town_or_village__icontains=term)
                                         | Q(nationality__icontains=term))
                if not results:
                    return Response({'results': 'Your search was not found', 'response_code': '101'},
                                    status=status.HTTP_400_BAD_REQUEST)
                serializer = GuardianSerializer(results, context={'request': request}, many=True)
                return Response({'results': serializer.data, 'response_code': '100'}, status=status.HTTP_200_OK)
            return Response({'detail': 'Search Text is empty', 'response_code': '102'},
                            status=status.HTTP_400_BAD_REQUEST)


class CenterBirthSearch(APIView):
    permission_classes = (CenterUserPermissions, )

    def post(self, request):
        search = request.data.get('search', None)

        if search is not None:
            center_id = self.request.user.center_id
            results = Birth.objects.filter(created_by_center__is_active=True, created_by_center__id=center_id)
            for term in search.split():
                results = results.filter(Q(first_name__icontains=term)
                                         | Q(last_name__icontains=term)
                                         | Q(id__icontains=term)
                                         | Q(born_at__icontains=term)
                                         )
                if not results:
                    return Response({'results': 'Your search was not found', 'response_code': '101'},
                                    status=status.HTTP_400_BAD_REQUEST)
                serializer = BirthSerializer(results, context={'request': request}, many=True)
                return Response({'results': serializer.data, 'response_code': '100'}, status=status.HTTP_200_OK)
            return Response({'detail': 'Search Text is empty', 'response_code': '102'},
                            status=status.HTTP_400_BAD_REQUEST)


class BirthSearch(APIView):
    permission_classes = (RegistrarUserPermissions, )

    def post(self, request):
        search = request.data.get('search', None)
        date_filter = request.data.get('date_filter', None)
        date_filter = date_filter + '__range' if date_filter is not None else date_filter
        start_date = request.data.get('start_date', None)
        end_date = request.data.get('end_date', None)
        sex = request.data.get('sex', None)

        print(date_filter)
        print(sex)
        print(search)

        results = Birth.objects.all()

        if search is None:
            if date_filter is None and sex is not None:
                results = results.filter(Q(sex__exact=sex)
                                         )

                if not results:
                    return Response({'results': 'Your search was not found', 'response_code': '101'},
                                    status=status.HTTP_400_BAD_REQUEST)
                serializer = BirthCertSerializer(results, context={'request': request}, many=True)
                return Response({'results': serializer.data, 'response_code': '100'}, status=status.HTTP_200_OK)
            elif date_filter is not None and sex is None:
                results = results.filter(Q(**{date_filter: [start_date, end_date]})
                                         )

                if not results:
                    return Response({'results': 'Your search was not found', 'response_code': '101'},
                                    status=status.HTTP_400_BAD_REQUEST)
                serializer = BirthCertSerializer(results, context={'request': request}, many=True)
                return Response({'results': serializer.data, 'response_code': '100'}, status=status.HTTP_200_OK)
            elif date_filter is not None and sex is not None:
                results = results.filter(Q(sex__exact=sex)
                                         & Q(**{date_filter: [start_date, end_date]})
                                         )

                if not results:
                    return Response({'results': 'Your search was not found', 'response_code': '101'},
                                    status=status.HTTP_400_BAD_REQUEST)
                serializer = BirthCertSerializer(results, context={'request': request}, many=True)
                return Response({'results': serializer.data, 'response_code': '100'}, status=status.HTTP_200_OK)
        elif search is not None and (date_filter is None and sex is None):
            for term in search.split():
                results = results.filter(Q(first_name__icontains=term)
                                         | Q(last_name__icontains=term)
                                         | Q(id__icontains=term)
                                         | Q(born_at__icontains=term)
                                         | Q(created_by__in=term)
                                         | Q(created_by_center__center_name__icontains=term)
                                         | Q(born_at__icontains=term)
                                         | Q(sex__exact=term)
                                         )
                if not results:
                    return Response({'results': 'Your search was not found', 'response_code': '101'},
                                    status=status.HTTP_400_BAD_REQUEST)
                serializer = BirthCertSerializer(results, context={'request': request}, many=True)
                return Response({'results': serializer.data, 'response_code': '100'}, status=status.HTTP_200_OK)
            return Response({'detail': 'Search Text is empty', 'response_code': '102'},
                            status=status.HTTP_400_BAD_REQUEST)
        elif search is not None and (date_filter is not None or sex is not None):
            for term in search.split():
                results = results.filter(Q(first_name__icontains=term)
                                         | Q(last_name__icontains=term)
                                         | Q(id__icontains=term)
                                         | Q(born_at__icontains=term)
                                         | Q(created_by__in=term)
                                         | Q(created_by_center__center_name__icontains=term)
                                         | Q(born_at__icontains=term)
                                         | Q(sex__exact=sex)
                                         | Q(**{date_filter: [start_date, end_date]})
                                         )
                if not results:
                    return Response({'results': 'Your search was not found', 'response_code': '101'},
                                    status=status.HTTP_400_BAD_REQUEST)
                serializer = BirthCertSerializer(results, context={'request': request}, many=True)
                return Response({'results': serializer.data, 'response_code': '100'}, status=status.HTTP_200_OK)
            return Response({'detail': 'Search Text is empty', 'response_code': '102'},
                            status=status.HTTP_400_BAD_REQUEST)


class CreateCenterGuardian(CreateAPIView):
    permission_classes = (CenterUserPermissions,)
    serializer_class = GuardianSerializer
    queryset = Guardian.objects.all()

    def create(self, request, *args, **kwargs):
        guardian = request.data.get('guardian', None)
        first_name = request.data.get('first_name', None)
        last_name = request.data.get('last_name', None)
        other_name = request.data.get('other_name', None)
        date_of_birth = request.data.get('date_of_birth', None)
        children = request.data.get('number_of_children', None)
        nationality = request.data.get('nationality', None)
        religion = request.data.get('religion', None)
        contact_info = request.data.get('contact_info', None)
        home_address = request.data.get('home_address', None)
        town_or_village = request.data.get('town_or_village', None)
        region = request.data.get('region', None)
        level_of_education = request.data.get('level_of_education', None)
        center_id = self.request.user.center_id

        guardian = Guardian.objects.create(
            guardian=guardian,
            first_name=first_name,
            last_name=last_name,
            other_name=other_name,
            date_of_birth=date_of_birth,
            number_of_children=children,
            nationality=nationality,
            religion=religion,
            contact_info=contact_info,
            home_address=home_address,
            town_or_village=town_or_village,
            region=region,
            level_of_education=level_of_education,
            created_by_center=MedicalCenter.objects.get(id=center_id)
        )
        serializer = GuardianSerializer(guardian, context={'request': request}, many=False)
        return Response({'results': serializer.data, 'response_code': '100'}, status=status.HTTP_201_CREATED)


class CreateRegistrarGuardian(CreateAPIView):
    permission_classes = (RegistrarUserPermissions,)
    serializer_class = GuardianSerializer
    queryset = Guardian.objects.all()

    def create(self, request, *args, **kwargs):
        guardian = request.data.get('guardian', None)
        first_name = request.data.get('first_name', None)
        last_name = request.data.get('last_name', None)
        other_name = request.data.get('other_name', None)
        date_of_birth = request.data.get('date_of_birth', None)
        children = request.data.get('number_of_children', None)
        nationality = request.data.get('nationality', None)
        religion = request.data.get('religion', None)
        contact_info = request.data.get('contact_info', None)
        home_address = request.data.get('home_address', None)
        town_or_village = request.data.get('town_or_village', None)
        region = request.data.get('region', None)
        level_of_education = request.data.get('level_of_education', None)
        guardian = Guardian.objects.create(
            guardian=guardian,
            first_name=first_name,
            last_name=last_name,
            other_name=other_name,
            date_of_birth=date_of_birth,
            number_of_children=children,
            nationality=nationality,
            religion=religion,
            contact_info=contact_info,
            home_address=home_address,
            town_or_village=town_or_village,
            region=region,
            level_of_education=level_of_education,
            created_by=BRUser.objects.get(user_id=self.request.user.user_id)
        )
        serializer = GuardianSerializer(guardian, context={'request': request}, many=False)
        return Response({'results': serializer.data, 'response_code': '100'}, status=status.HTTP_201_CREATED)


class CenterGuardianList(ListAPIView):
    permission_classes = (CenterUserPermissions,)
    serializer_class = GuardianSerializer
    queryset = Guardian.objects.all()

    def list(self, request, *args, **kwargs):
        center_id = self.request.user.center_id
        queryset = Guardian.objects.filter(created_by_center__is_active=True, created_by_center__id=center_id)
        serializer = GuardianSerializer(queryset, context={'request': request}, many=True)
        return Response({'results': serializer.data, 'response_code': '100'}, status=status.HTTP_200_OK)


class AllGuardianList(ListAPIView):
    permission_classes = (RegistrarUserPermissions,)
    serializer_class = GuardianSerializer
    queryset = Guardian.objects.all()

    def list(self, request, *args, **kwargs):
        queryset = Guardian.objects.all()
        serializer = GuardianSerializer(queryset, context={'request': request}, many=True)
        return Response({'results': serializer.data, 'response_code': '100'}, status=status.HTTP_200_OK)


class CenterGuardianUpdate(UpdateAPIView):
    permission_classes = (CenterUserPermissions,)
    serializer_class = GuardianSerializer
    lookup_field = 'id'

    def get_queryset(self):
        return Guardian.objects.all()

    def put(self, request, *args, **kwargs):
        return self.update(request, *args, **kwargs)

    def patch(self, request, *args, **kwargs):
        return self.partial_update(request, *args, **kwargs)


class RegistrarGuardianUpdate(UpdateAPIView):
    permission_classes = (RegistrarUserPermissions,)
    serializer_class = GuardianSerializer
    lookup_field = 'id'

    def get_queryset(self):
        return Guardian.objects.all()

    def put(self, request, *args, **kwargs):
        return self.update(request, *args, **kwargs)

    def patch(self, request, *args, **kwargs):
        return self.partial_update(request, *args, **kwargs)


class CreateDoctor(CreateAPIView):
    permission_classes = (IsAuthenticated, NurseUserPermissions, CenterAdminUserPermissions)
    serializer_class = DoctorSerializer
    queryset = Doctor.objects.all()

    def create(self, request, *args, **kwargs):
        full_name = request.data.get('full_name', None)
        department = request.data.get('department', None)
        employee_number = request.data.get('employee_number', None)
        center_id = self.request.user.center_id

        doctor = Doctor.objects.create(
            full_name=full_name,
            department=department,
            employee_number=employee_number,
            created_by=MedicalCenter.objects.get(id=center_id)
        )
        serializer = DoctorSerializer(doctor, context={'request': request}, many=False)
        return Response({'results': serializer.data, 'response_code': 100}, status=status.HTTP_201_CREATED)


class DoctorList(ListAPIView):
    permission_classes = (IsAuthenticated, CenterAdminUserPermissions)
    serializer_class = DoctorSerializer
    queryset = Doctor.objects.all()

    def list(self, request, *args, **kwargs):
        center_id = self.request.user.center_id
        queryset = Doctor.objects.filter(created_by__is_active=True, created_by__id=center_id)
        serializer = DoctorSerializer(queryset, context={'request': request}, many=True)
        return Response({'results': serializer.data, 'response_code': '100'}, status=status.HTTP_200_OK)


class DoctorUpdate(UpdateAPIView):
    permission_classes = (IsAuthenticated, CenterAdminUserPermissions)
    serializer_class = DoctorSerializer
    lookup_field = 'id'

    def get_queryset(self):
        center_id = self.request.user.center_id
        return Doctor.objects.filter(created_by__is_active=True, created_by__id=center_id)

    def update(self, request, *args, **kwargs):
        try:
            obj = self.get_object()
            data = request.data
            serializer = DoctorSerializer(obj, context={'request': request}, data=data, partial=True)
            if serializer.is_valid():
                serializer.save()
            return Response({'results': serializer.data, 'response_code': '100'}, status=status.HTTP_200_OK)
        except Exception:
            return Response({'detail': 'Request for not owned object', 'response_code': '101'},
                            status=status.HTTP_400_BAD_REQUEST)


class CreateCenterBirth(CreateAPIView):
    permission_classes = (CenterUserPermissions,)
    serializer_class = BirthCertSerializer
    queryset = Birth.objects.all()

    def create(self, request, *args, **kwargs):
        center_id = request.data.get('center_id', None)

        # Father
        full_name = request.data.get('father_name', None)
        nationality = request.data.get('father_nationality', None)
        religion = request.data.get('father_religion', None)
        contact_info = request.data.get('father_contact', None)
        occupation = request.data.get('father_occupation', None)
        father = Guardian.objects.create(
            guardian='Father',
            full_name=full_name,
            nationality=nationality,
            religion=religion,
            contact_info=contact_info,
            occupation=occupation,
            created_by=self.request.user,
        )
        father.save()

        # Mother
        maiden_name = request.data.get('mother_maiden_name', None)
        nationality = request.data.get('mother_nationality', None)

        mother = Guardian.objects.create(
            guardian='Mother',
            maiden_name=maiden_name,
            nationality=nationality,
            created_by_center=MedicalCenter.objects.get(id=center_id),
            created_by=self.request.user,
        )
        mother.save()

        first_name = request.data.get('first_name', None)
        last_name = request.data.get('last_name', None)
        other_name = request.data.get('other_name', None)
        sex = request.data.get('sex', None)
        date_of_birth = request.data.get('date_of_birth', None)
        born_at = request.data.get('place_of_birth', None)
        informant = request.data.get('informant', None)
        informant_contact = request.data.get('informant_contact', None)
        informant_relation = request.data.get('informant_relation', None)

        birth = Birth.objects.create(
            first_name=first_name,
            last_name=last_name,
            other_name=other_name,
            informant=informant,
            informant_contact=informant_contact,
            informant_relation=informant_relation,
            sex=sex,
            parents1=Guardian.objects.get(id=father.id),
            parents2=Guardian.objects.get(id=mother.id),
            date_of_birth=date_of_birth,
            born_at=born_at,
            created_by_center=MedicalCenter.objects.get(id=center_id),
            created_by=self.request.user,
        )
        birth.save()
        # myfather = GuardianSerializer(father, context={'request': request}, many=False)
        # mymother = GuardianSerializer(mother, context={'request': request}, many=False)
        # mybirth = BirthSerializer(birth, context={'request': request}, many=False)
        # 'birth': mybirth.data, 'mother': mymother.data, 'father': myfather.data,
        return Response({'response_code': 100}, status=status.HTTP_201_CREATED)


class CreateRegistrarBirth(CreateAPIView):
    permission_classes = (RegistrarUserPermissions,)
    serializer_class = BirthCertSerializer
    queryset = Birth.objects.all()

    def create(self, request, *args, **kwargs):
        center_id = request.data.get('center_id', None)

        # Father
        full_name = request.data.get('father_name', None)
        father_nationality = request.data.get('father_nationality', None)
        religion = request.data.get('father_religion', None)
        contact_info = request.data.get('father_contact', None)
        occupation = request.data.get('father_occupation', None)
        father = Guardian.objects.create(
            guardian='Father',
            full_name=full_name,
            nationality=father_nationality,
            religion=religion,
            contact_info=contact_info,
            occupation=occupation,
            created_by_center=MedicalCenter.objects.get(id=center_id),
            created_by=self.request.user,
        )
        father.save()

        # Mother
        maiden_name = request.data.get('mother_maiden_name', None)
        nationality = request.data.get('mother_nationality', None)

        mother = Guardian.objects.create(
            guardian='Mother',
            maiden_name=maiden_name,
            nationality=nationality,
            created_by_center=MedicalCenter.objects.get(id=center_id),
            created_by=self.request.user,
        )
        mother.save()

        first_name = request.data.get('first_name', None)
        last_name = request.data.get('last_name', None)
        other_name = request.data.get('other_name', None)
        sex = request.data.get('sex', None)
        date_of_birth = request.data.get('date_of_birth', None)
        born_at = request.data.get('place_of_birth', None)
        informant = request.data.get('informant', None)
        informant_contact = request.data.get('informant_contact', None)
        informant_relation = request.data.get('informant_relation', None)

        birth = Birth.objects.create(
            first_name=first_name,
            last_name=last_name,
            other_name=other_name,
            informant=informant,
            informant_contact=informant_contact,
            informant_relation=informant_relation,
            sex=sex,
            date_of_birth=date_of_birth,
            born_at=born_at,
            created_by_center=MedicalCenter.objects.get(id=center_id),
            parents1=Guardian.objects.get(id=father.id),
            parents2=Guardian.objects.get(id=mother.id),
            created_by=self.request.user,
        )
        birth.save()

        # myfather = GuardianSerializer(father, context={'request': request}, many=False)
        # mymother = GuardianSerializer(mother, context={'request': request}, many=False)
        # mybirth = BirthSerializer(birth, context={'request': request}, many=False)
        # 'birth': mybirth.data, 'mother': mymother.data, 'father': myfather.data,
        return Response({'response_code': 100}, status=status.HTTP_201_CREATED)


class CenterBirthList(ListAPIView):
    permission_classes = (CenterUserPermissions,)
    serializer_class = BirthCertSerializer
    queryset = Birth.objects.all()

    def list(self, request, *args, **kwargs):
        center_id = self.request.user.center_id
        queryset = Birth.objects.filter(created_by_center__is_active=True, created_by_center__id=center_id)
        serializer = BirthCertSerializer(queryset, context={'request': request}, many=True)
        return Response({'results': serializer.data, 'response_code': '100'}, status=status.HTTP_200_OK)


class AllBirthList(ListAPIView):
    permission_classes = (AllowAny,)
    serializer_class = BirthCertSerializer
    queryset = Birth.objects.all()

    def list(self, request, *args, **kwargs):
        queryset = Birth.objects.all()
        serializer = BirthCertSerializer(queryset, context={'request': request}, many=True)
        return Response({'results': serializer.data, 'response_code': '100'}, status=status.HTTP_200_OK)


class BirthCert(ListAPIView):
    permission_classes = (AllowAny,)
    serializer_class = BirthCertSerializer
    queryset = Birth.objects.all()


class BirthList(ListAPIView):
    permission_classes = (RegistrarUserPermissions,)
    serializer_class = BirthCertSerializer
    queryset = Birth.objects.all()


class CenterBirthDetail(RetrieveAPIView):
    permission_classes = (CenterUserPermissions,)
    serializer_class = BirthCertSerializer
    lookup_field = 'id'

    def get_queryset(self):
        center_id = self.request.user.center_id
        return Birth.objects.filter(created_by_center__is_active=True, created_by_center__id=center_id)

    def get(self, request, *args, **kwargs):
        return self.retrieve(request, *args, **kwargs)


class RegistrarBirthDetail(RetrieveAPIView):
    permission_classes = (RegistrarUserPermissions,)
    serializer_class = BirthCertSerializer
    lookup_field = 'id'

    def get_queryset(self):
        # center_id = self.request.user.center_id
        return Birth.objects.all()

    def get(self, request, *args, **kwargs):
        return self.retrieve(request, *args, **kwargs)


class CenterBirthUpdate(UpdateAPIView):
    permission_classes = (CenterUserPermissions,)
    serializer_class = BirthSerializer
    queryset = Birth.objects.all()
    lookup_field = 'id'

    def put(self, request, *args, **kwargs):
        return self.update(request, *args, **kwargs)

    def patch(self, request, *args, **kwargs):
        Guardian.objects.filter(pk=request.data.get('mother_id')).update(
            maiden_name=request.data.get('mother_maiden_name'),
            nationality=request.data.get('mother_nationality'),
        )

        Guardian.objects.filter(pk=request.data.get('father_id')).update(
            full_name=request.data.get('father_name'),
            nationality=request.data.get('father_nationality'),
            occupation=request.data.get('father_occupation'),
            religion=request.data.get('father_religion'),
            contact_info=request.data.get('father_contact')
        )
        return self.partial_update(request, *args, **kwargs)

    # def put(self, request, *args, **kwargs):
    #     try:
    #         obj = self.get_object()
    #         data = request.data
    #         serializer = BirthSerializer(obj, context={'request': request}, data=data, partial=True)
    #         if serializer.is_valid():
    #             serializer.save()
    #         return Response({'results': serializer.data, 'response_code': '100'}, status=status.HTTP_200_OK)
    #     except Exception:
    #         return Response({'detail': 'Request for not owned object', 'response_code': '101'},
    #                         status=status.HTTP_400_BAD_REQUEST)
    #
    # def patch(self, request, *args, **kwargs):
    #     try:
    #         obj = self.get_object()
    #         data = request.data
    #         serializer = BirthSerializer(obj, context={'request': request}, data=data, partial=True)
    #         if serializer.is_valid():
    #             serializer.save()
    #         return Response({'results': serializer.data, 'response_code': '100'}, status=status.HTTP_200_OK)
    #     except Exception:
    #         return Response({'detail': 'Request for not owned object', 'response_code': '101'},
    #                         status=status.HTTP_400_BAD_REQUEST)


class RegistrarBirthUpdate(UpdateAPIView):
    permission_classes = (RegistrarUserPermissions,)
    serializer_class = BirthSerializer
    queryset = Birth.objects.all()
    lookup_field = 'id'

    def put(self, request, *args, **kwargs):
        return self.update(request, *args, **kwargs)

    def patch(self, request, *args, **kwargs):
        Guardian.objects.filter(pk=request.data.get('mother_id')).update(
            maiden_name=request.data.get('mother_maiden_name'),
            nationality=request.data.get('mother_nationality'),
            age=request.data.get('mother_age'),
            level_of_education=request.data.get('mother_level_of_education'),
            occupation=request.data.get('mother_occupation'),
        )

        Guardian.objects.filter(pk=request.data.get('father_id')).update(
            full_name=request.data.get('father_name'),
            nationality=request.data.get('father_nationality'),
            occupation=request.data.get('father_occupation'),
            religion=request.data.get('father_religion'),
            contact_info=request.data.get('father_contact'),
            age=request.data.get('father_age'),
            level_of_education=request.data.get('father_level_of_education'),
        )

        return self.partial_update(request, *args, **kwargs)


class FilterBirth(ListAPIView):
    queryset = Birth.objects.all()
    serializer_class = BirthSerializer
    filter_backends = (filters.DjangoFilterBackend,)
    filterset_class = BirthFilter
    permission_classes = (RegistrarUserPermissions,)


class GenerateBirthCert(UpdateAPIView):
    permission_classes = (AllowAny,)
    serializer_class = BirthCertSerializer
    queryset = Birth.objects.all()
    lookup_field = 'id'

    def patch(self, request, *args, **kwargs):
        birth = Birth.objects.get(id=request.data.get('birth_record_id'))

        sms = SMS(client_id='hxkmagyr', client_secret='rifpeluf')
        message = Message(
            sender='BirthReg',
            content=f"Hello {birth.informant}, your birth registration is complete. Kindly visit your centre of"
                    f"registration for your certificate a week from now.",
            recipient=birth.informant_contact,
            registered_delivery=True
        )
        sms.send(message)

        return self.partial_update(request, *args, **kwargs)


class ShowBirthStats(APIView):
    permission_classes = (RegistrarUserPermissions,)
    serializer_class = BirthCertSerializer
    lookup_field = 'id'

    def get_queryset(self):
        # center_id = self.request.user.center_id
        return Birth.objects.all()

    def post(self, request, *args, **kwargs):
        start_date = request.data.get('date_start')
        end_date = request.data.get('date_end')
        numOfBirthRecords = len(Birth.objects.filter(created_at__range=[start_date, end_date]))
        numOfBirthMaleRecords = len(Birth.objects.filter(sex='Male', created_at__range=[start_date, end_date]))
        numOfBirthFemaleRecords = len(Birth.objects.filter(sex='Female', created_at__range=[start_date, end_date]))

        return Response({'num_birth_records': numOfBirthRecords, 'num_male_birth_records': numOfBirthMaleRecords,
                         'num_female_birth_records': numOfBirthFemaleRecords})

        # return self.retrieve(request, *args, **kwargs)