from rest_framework import serializers
from .models import *
from django_filters import rest_framework as filters


class BirthFilter(filters.FilterSet):
    start_date = filters.DateFilter(field_name='date_of_birth', lookup_expr='gt')
    end_date = filters.DateFilter(field_name='date_of_birth', lookup_expr='lt')
    date_range = filters.DateRangeFilter(field_name='date_of_birth')

    class Meta:
        model = Birth
        fields = ('sex', 'created_by_center', 'first_name', 'last_name',)


class BirthCertSerializer(serializers.ModelSerializer):
    # full_name = serializers.ReadOnlyField(source='get_full_name')
    branch = serializers.ReadOnlyField(source='created_by.branch')
    father_name = serializers.ReadOnlyField(source='parents1.get_full_name')
    father_occupation = serializers.ReadOnlyField(source='parents1.occupation')
    father_nationality = serializers.ReadOnlyField(source='parents1.nationality')
    father_religion = serializers.ReadOnlyField(source='parents1.religion')
    # mother_name = serializers.ReadOnlyField(source='parents2.get_full_name')
    mother_maiden_name = serializers.ReadOnlyField(source='parents2.maiden_name')
    mother_occupation = serializers.ReadOnlyField(source='parents2.occupation')
    mother_nationality = serializers.ReadOnlyField(source='parents2.nationality')
    # mother_religion = serializers.ReadOnlyField(source='parents2.religion')
    father_contact = serializers.ReadOnlyField(source='parents1.contact_info')
    where_born = serializers.ReadOnlyField(source='created_by_center.center_name')
    center_id = serializers.ReadOnlyField(source='created_by_center.id')
    mother_id = serializers.ReadOnlyField(source='parents2.id')
    father_id = serializers.ReadOnlyField(source='parents1.id')
    mother_level_of_education = serializers.ReadOnlyField(source='parents2.level_of_education')
    father_level_of_education = serializers.ReadOnlyField(source='parents1.level_of_education')
    mother_age = serializers.ReadOnlyField(source='parents2.age')
    father_age = serializers.ReadOnlyField(source='parents1.age')

    class Meta:
        model = Birth
        fields = ('id', 'branch', 'first_name', 'last_name', 'other_name', 'sex', 'father_name', 'father_occupation',
                  'father_nationality', 'father_religion', 'born_at', 'mother_nationality',
                  'date_of_birth', 'where_born', 'date_registered', 'registered_by', 'is_registered',
                  'is_issued', 'date_issued', 'father_contact', 'informant', 'informant_contact',
                  'informant_relation', 'mother_maiden_name', 'center_id', 'mother_id', 'father_id', 'update_reason',
                  'registry_center', 'margin', 'issued_by', 'registry_center_district', 'type_of_birth',
                  'mother_level_of_education', 'father_level_of_education', 'mother_age', 'father_age',
                  'mother_occupation', 'created_at')


class BirthSerializer(serializers.ModelSerializer):
    created_by_center = serializers.ReadOnlyField(source='created_by_center.center_name')
    created_by = serializers.ReadOnlyField(source='created_by.get_full_name')
    mother = serializers.ReadOnlyField(source='parents2.get_full_name')
    father = serializers.ReadOnlyField(source='parents1.get_full_name')

    class Meta:
        model = Birth
        fields = ('id', 'first_name', 'last_name', 'other_name', 'sex', 'date_of_birth', 'born_at', 'father',
                  'mother', 'created_by_center', 'created_by', 'created_at', 'informant', 'informant_contact',
                  'informant_relation', 'update_reason', 'type_of_birth')
        read_only_fields = ('id', 'created_by_center', 'created_by', 'created_at', 'father', 'mother')

    # first_name = first_name,
    # last_name = last_name,
    # other_name = other_name,
    # informant = informant,
    # informant_contact = informant_contact,
    # informant_relation = informant_relation,
    # sex = sex,
    # date_of_birth = date_of_birth,
    # born_at = born_at,
    # created_by_center = MedicalCenter.objects.get(id=center_id),
    # parents1 = Guardian.objects.get(id=father.id),
    # parents2 = Guardian.objects.get(id=mother.id),
    # created_by = self.request.user,
    # def get_sex_display(self, obj):
    #     return obj.get_sex_display()

    # def get_parents1_id_display(self, obj):
    #     return obj.get_parent1_display()
    #
    # def get_parents2_id_display(self, obj):
    #     return obj.get_parent2_display()


class DoctorSerializer(serializers.ModelSerializer):

    class Meta:
        model = Doctor
        fields = ('id', 'full_name', 'department', 'employee_number', 'created_by', 'created_at')
        read_only_fields = ('id', 'created_by', 'created_at')


class GuardianSerializer(serializers.ModelSerializer):
    created_by_center = serializers.ReadOnlyField(source='created_by_center.center_name')
    created_by = serializers.ReadOnlyField(source='created_by.get_full_name')
    full_name = serializers.ReadOnlyField(source='get_full_name')

    class Meta:
        model = Guardian
        fields = ('id', 'guardian', 'full_name',  'nationality', 'religion', 'contact_info', 'created_by_center', 'created_by', 'created_at')
        read_only_fields = ('id', 'created_by', 'created_by_center', 'created_at')


class CenterSerializer(serializers.ModelSerializer):
    center_type_display = serializers.SerializerMethodField
    created_by = serializers.ReadOnlyField(source='created_by.get_full_name')

    class Meta:
        model = MedicalCenter
        fields = ('id', 'center_name', 'center_type', 'physical_address', 'registration_number', 'region', 'district', 'center_logo', 'created_by', 'created_at')
        read_only_fields = ('id', 'created_by', 'created_at')

    def get_center_type_display(self, obj):
        return obj.get_center_type_display()
