"""birth_register_project URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from rest_framework_jwt.views import obtain_jwt_token

from user import urls as user_urls
from birth import urls as birth_urls

apipatterns = [
    path('user/', include(user_urls)),
    path('birth/', include(birth_urls)),
]

urlpatterns = [
    path('api/', include(apipatterns)),
    path('admin/', admin.site.urls),
    path('auth/token/', obtain_jwt_token),
]
